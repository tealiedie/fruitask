﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class Spawners : MonoBehaviour {
	public GameObject currentFruit;
	public GameObject container;
	public List<GameObject> fruits = new List<GameObject> ();
	public BasketDirection basketDirection;
	public GameManager gameManager;
	public Vector2 nextPos, curPos;
	public float basketSpeed = 1f;
	public bool canMove;
	public RectTransform rt;
	public GameObject[] nextFruitsSpawner;
	bool collidedOnEdge;
	void Awake()
	{
		var index = Random.Range (0, fruits.Count);
		SpawnFruit ( fruits[index] );
	}
	void Start()
	{
		Invoke ("MoveToGrid", 2);
		curPos = transform.localPosition;
		nextPos = curPos;
	}
	public void SpawnFruit(GameObject fruit = null)
	{
		if (!fruit) {
			fruit = GetNewFruit ();
		}
		currentFruit = fruit;
		GameObject fruitInstane = Instantiate (fruit,transform.position, Quaternion.identity, container.transform) as GameObject;
		Fruit _fruit = fruitInstane.GetComponent<Fruit>();
		_fruit.instantiated = true;

	}

	GameObject GetNewFruit()
	{
		var index = Random.Range (0, fruits.Count);
		return fruits [index];
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "BasketCollider") {
			if (!collidedOnEdge) {
				collidedOnEdge = true;
				basketDirection = other.GetComponent<BasketCollider> ().basketDirection;
			}
		}
	}
	void OnTriggerExit2D(Collider2D other) {
		if (other.tag == "Fruits") {
			currentFruit = null;
			var index = Random.Range (0, fruits.Count);
			StartCoroutine (SpawnFruitAtHand ());
		}
	}
	IEnumerator SpawnFruitAtHand()
	{
		yield return new WaitForSeconds (1);
		SpawnFruit ();
	}

	void Update()
	{
		transform.localPosition = nextPos;
	}

	void MoveToGrid() {
		canMove = true;
		if (canMove) {
			if (basketDirection == BasketDirection.Left) {
				
				if (collidedOnEdge) {
					collidedOnEdge = false;
					nextPos.y -= rt.rect.height;
					gameManager.fruitDirection = FruitDirection.Up;
				}
				nextPos = new Vector2 (curPos.x - rt.rect.width, nextPos.y);
			} else if (basketDirection == BasketDirection.Up) {
				if (collidedOnEdge) {
					collidedOnEdge = false;
					nextPos.x -= rt.rect.width;
					gameManager.fruitDirection = FruitDirection.Right;
				}
				nextPos = new Vector2 (nextPos.x, curPos.y + rt.rect.height);
			} else if (basketDirection == BasketDirection.Right) {

				if (collidedOnEdge) {
					collidedOnEdge = false;
					nextPos.y += rt.rect.height;
					gameManager.fruitDirection = FruitDirection.Bottom;
				}
				nextPos = new Vector2 (curPos.x + rt.rect.width, nextPos.y);
			} else if (basketDirection == BasketDirection.Bottom) {

				if (collidedOnEdge) {
					collidedOnEdge = false;
					nextPos.x += rt.rect.width;
					gameManager.fruitDirection = FruitDirection.Left;
				}
				nextPos = new Vector2 (nextPos.x, curPos.y - rt.rect.height);
			}
			curPos = nextPos;
			canMove = false;
		}
		Invoke ("MoveToGrid", basketSpeed);
	}
}

public enum BasketDirection {
	Left,
	Up,
	Right,
	Bottom,
}