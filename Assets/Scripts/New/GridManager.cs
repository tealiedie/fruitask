﻿using UnityEngine;
using System.Collections.Generic;

public class GridManager : MonoBehaviour
{
	// A simple class to handle the coordinates.
	public class XY 
	{
		public int X;
		public int Y;

		public XY (int x, int y)
		{
			X = x;
			Y = y;
		}
	}

	// The tile class. Keeps track of the tile type, the GameObject, and its controller.
	public class Tile
	{
		public int TileType;
		public GameObject GO;
		public TileControl TileControl;

		public Tile ()
		{            
			TileType = -1;            
		}

		public Tile (int tileType, GameObject go, TileControl tileControl)
		{
			TileType = tileType;
			GO = go;
			TileControl = tileControl;
		}
	}

	public GameObject[] TilePrefabs;

	public int GridWidth;
	public int GridHeight;
	public Tile[,] Grid;

	private int movingTiles;

	void Awake()
	{
		CreateGrid();
	}

	void CreateGrid()
	{
		Grid = new Tile[GridWidth, GridHeight];        

		for (int x = 0; x < GridWidth; x++)
		{
			for (int y = 0; y < GridHeight; y++)
			{
				int randomTileType = Random.Range(0, TilePrefabs.Length);
				GameObject go = Instantiate(TilePrefabs[randomTileType], new Vector2(x, y), Quaternion.identity) as GameObject;
				TileControl tileControl = go.GetComponent<TileControl>();             
				Grid[x, y] = new Tile(randomTileType, go, tileControl);
				tileControl.GridManager = this;
				tileControl.MyXY = new XY(x, y);
				go.name = x + "/" + y;
			}
		} 
	}
}