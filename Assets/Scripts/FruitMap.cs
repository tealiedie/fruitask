﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FruitMap : MonoBehaviour {
	public bool initialized;
	public GameObject fruitContainerPrefab;
	public GameObject fruitPanel;
	public List<GameObject> fruitContainers = new List<GameObject>();
	public List<GameObject> fruits = new List<GameObject> ();
	public int row = 11;
	public int col = 6;
	public MinMaxSpawns minMaxSpawns;
	public float startIndexRate = 0.7f;
	RectTransform fruitsContainerPanelTransform;
	GameObject fruitsContainerPanel;
	public bool isChecking;
	GameManager gameManager;

	public void CheckCollisions(int _Row,int _Column)
	{
		
	}

	void Start()
	{
		if (!gameManager) {
			gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		}

		InitMap ();
		Invoke("PlaceStarterFruits",.5f);
		initialized = true;
	}

	void PlaceStarterFruits()
	{
		int maxStarterFruits = Random.Range (minMaxSpawns.min, minMaxSpawns.max);
		int maxHalf = Mathf.FloorToInt (maxStarterFruits / 2);
		int startPosX = Random.Range ((int) Mathf.Floor (col / 2), (int) Mathf.Ceil (col / 2) );
		int startPosY = Random.Range ((int) Mathf.Floor (row / 2), (int) Mathf.Ceil (row / 2) );
		int startIndex = ((startPosX - 1) * row) + (startPosY-1);
		int startIndexAdd = Mathf.FloorToInt (row * startIndexRate);
		GameObject starterContainer = fruitContainers [ startIndex ];
		for (int i = 0; i < maxStarterFruits; i++) {
			FruitContainer fruitContainer = starterContainer.GetComponent<FruitContainer> ();
			RectTransform starterRT = starterContainer.GetComponent<RectTransform> ();
			int fruitIndex = Random.Range(0,fruits.Count-1);
			GameObject fruit = fruits[fruitIndex];

			var _fruit = Instantiate(fruit,starterContainer.transform) as GameObject;
			Fruit fruitScript = _fruit.GetComponent<Fruit> ();
			fruitScript.Row = fruitContainer.Row;
			fruitScript.Column = fruitContainer.Column;

			RectTransform _fruitRT = _fruit.GetComponent<RectTransform> ();
			_fruitRT.anchoredPosition = Vector2.zero;
			_fruitRT.sizeDelta = starterRT.sizeDelta;

			BoxCollider2D _fruitBC = _fruit.GetComponent<BoxCollider2D> ();
			_fruitBC.size = starterRT.sizeDelta;

//			_fruit.transform.SetParent(transform);

			if (gameManager) {
				gameManager.fruits.Add (_fruit);
			}

			if (i == maxHalf)
				startIndex += startIndexAdd;
			
			starterContainer = fruitContainers [ ++startIndex ];

		}
	}

	GameObject FindFruitContainer(int _Column, int _Row)
	{
		GameObject _fruitContainer = null;
		foreach (GameObject fruitContainer in fruitContainers) {
			FruitContainer fr = fruitContainer.GetComponent<FruitContainer> ();
			if (_Column == fr.Column && _Row == fr.Row) {
				_fruitContainer = fruitContainer;
			}
		}
		return _fruitContainer;
	}


	void InitMap()
	{
		
		fruitsContainerPanel = gameObject;
		fruitsContainerPanelTransform = fruitsContainerPanel.GetComponent<RectTransform> ();
		Vector2 wrapperSize = new Vector2 (fruitsContainerPanelTransform.rect.width / col, fruitsContainerPanelTransform.rect.height / row);
		Vector2 boxColliderSize = wrapperSize;
		Vector2 boxColliderOffset = new Vector2(wrapperSize.x / 2, (wrapperSize.y / 2)*-1 );
		float containerX = 0;
		float containerY = 0;
		for (int c = 1; c <= col; c++) {
			for (int r = 1; r <= row; r++) {
				var container = Instantiate (fruitContainerPrefab, gameObject.transform) as GameObject;
				FruitContainer fruitContainerScript = container.GetComponent<FruitContainer> ();
				fruitContainerScript.Row = r;
				fruitContainerScript.Column = c;
				RectTransform containerTransform = container.GetComponent<RectTransform> ();
				containerTransform.localPosition = new Vector3 (containerX, containerY, 0);
				containerTransform.anchoredPosition = new Vector2 (containerX, containerY);
				containerTransform.sizeDelta = wrapperSize;
				containerTransform.localScale = new Vector2 (1, 1);

				BoxCollider2D containerBoxCollider2D = container.GetComponent<BoxCollider2D> ();
				containerBoxCollider2D.size = new Vector2(10,10);
				containerBoxCollider2D.offset = boxColliderOffset;

				fruitContainers.Add (container);
				containerY -= wrapperSize.y;
			}
			containerY = 0;
			containerX += wrapperSize.x;
		}
	}
}
[System.Serializable]
public class MinMaxSpawns{
	public int min;
	public int max;
}

public enum FruitType {
	Apple,
	Kiwi,
	Lemon,
	Orange,
	Peach,
	Watermelon
};
