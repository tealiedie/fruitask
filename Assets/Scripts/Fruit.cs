﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class Fruit : MonoBehaviour {
	public FruitDirection fruitDirection;
	public bool attached = false;
	public Sprite wholeFruit;
	public FruitType fruitType;
	public List<Collider2D> colliders = new List<Collider2D>();
	public FruitStatus fruitStatus = FruitStatus.Half;

	private bool isWhole = false;

	private FruitMap fruitMap;
	private GameManager gameManager;
	private bool gameInitialized;
	private bool movingToDirection;
	public bool instantiated;
	public int Row, Column;
	Rigidbody2D rigidBody2D;
	public float bulletSpeed = 10;
	public bool startMoving = false;
	public GameObject fruitContainer;
	private bool fromBasket = false;
	public bool collidedWithSameFruit;
	private bool isChecking;
	public GameObject explosion;
	bool isSameFruitType(FruitType _fruitType) {
		return fruitType == _fruitType; 
	}
	bool isSameFruitStatus(FruitStatus _fruitStatus) {
		return fruitStatus == _fruitStatus;
	}
	public bool wasReleased = false;
	void Awake()
	{
		if (!rigidBody2D)
			rigidBody2D = gameObject.GetComponent<Rigidbody2D> ();
		
		if (!gameManager)
			gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}

	void Start()
	{
//		fruitDirection = gameManager.fruitDirection;
		fruitMap = GameObject.Find ("FruitsContainerPanel").GetComponent<FruitMap> ();
		gameInitialized = fruitMap.initialized;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "EdgeColliders") {
			Destroy (gameObject);
		}

		if (gameInitialized && other.tag == "FruitCollider") {
			startMoving = false;
			Fruit parentFruit = other.GetComponentInParent<Fruit> ();
			if ( isSameFruitType(parentFruit.fruitType) ) {
//				if (!fruitMap.isChecking) {
//					fruitMap.CheckCollisions (Row,Column);
////					gameManager.CheckCollisions ();
////					CheckHorizontalMatches(other.gameObject);
//				}
				if (!isChecking) {
					isChecking = true;
					CheckMatches (other.transform.parent.gameObject);
				}
			}
		}
		if (!startMoving && fromBasket) {
			startMoving = false;
			if (gameInitialized && other.tag == "FruitContainer") {
				fruitContainer = other.gameObject;
				attached = true;
//				SnapToGrid ();
			}	
		}

	}


	void CheckMatches(GameObject go)
	{
		Fruit _fruit = go.GetComponent<Fruit> ();
		GameObject _go = null;
		GameObject _otherGO = null;
		if ( isSameFruitStatus(_fruit.fruitStatus) ) {
			if (Row < _fruit.Row || Column < _fruit.Column) {
				_go = go;
				_otherGO = gameObject;
			} 
//			else(Row > _fruit.Row || Column > _fruit.Column) {
			else {
				_go = gameObject;
				_otherGO = go;
			}
//			Instantiate (explosion, _go.transform.position,Quaternion.identity,_go.transform);
			Destroy (_go);
			_otherGO.GetComponent<Fruit> ().fruitStatus = FruitStatus.Whole;
			_otherGO.GetComponent<Image> ().sprite = wholeFruit;
		}
	}



	public void SnapToGrid()
	{
		transform.SetParent(fruitContainer.transform);
	}

	void FixedUpdate()
	{
		fruitDirection = gameManager.fruitDirection;
		if (!startMoving && Input.GetMouseButtonDown (0) && instantiated && !attached) {
			startMoving = true;
			if (!fromBasket) {
				fromBasket = true;
			}
			if (!transform.IsChildOf (fruitMap.transform)) {
				transform.SetParent(fruitMap.transform);
			}
		}
		if (!attached && instantiated && startMoving) {
			Vector2 pos = transform.position;
			Vector2 dir = Vector2.zero;
			if (fruitDirection == FruitDirection.Up) {
				dir = pos + Vector2.up * bulletSpeed * Time.fixedDeltaTime;
			} else if (fruitDirection == FruitDirection.Right) {
				dir = pos + Vector2.right * bulletSpeed * Time.fixedDeltaTime;
			} else if (fruitDirection == FruitDirection.Left) {
				dir = pos + Vector2.left * bulletSpeed  * Time.fixedDeltaTime;
			} else if (fruitDirection == FruitDirection.Bottom) {
				dir = pos + Vector2.down * bulletSpeed * Time.fixedDeltaTime;
			}
			//			transform.position = dir;
			rigidBody2D.MovePosition (dir);
		}
	}
}

public enum FruitStatus {
	Half, Whole
}