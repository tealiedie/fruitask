﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class GameManager : MonoBehaviour {

	public List<GameObject> fruits = new List<GameObject>();
	public bool isChecking = false;
	public FruitDirection fruitDirection = FruitDirection.Up;
	public int halfMinimumMatches = 2;
	public int wholeMinimumMatches = 3;
	public void CheckCollisions()
	{
		isChecking = true;
		foreach (GameObject go in fruits) {
			Fruit _fruit = go.GetComponent<Fruit> ();
			List<Collider2D> colliders = _fruit.colliders;
		}
		Invoke ("StopChecking", 1);
	}

	void StopChecking()
	{
		isChecking = false;
	}
}

public enum FruitDirection {
	Right,
	Up,
	Bottom,
	Left
}